# Eticket

_Automatizar tickets con una interfaz amigable_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Debes tener isntalado docker y docker-compose en tu maquina para tener una copia del proyecto_

### Instalación 🔧

_Solo debes correr el docker compose del proyecto_


```
docker-compose up -d 
```

_Y ir a_

```
http://localhost:8000/
```

## Despliegue 📦

_Se recomienda hacer despligue con docker_

## Construido con 🛠️

_Este proyecto es una iniciativa constuida con python/django_

* [Django](https://www.djangoproject.com/) - El framework web usado
## Licencia 📄

Este proyecto está bajo la Licencia (GNU)



---
⌨️ con ❤️ por _Marcela beltran y Rafael barboza_ 😊