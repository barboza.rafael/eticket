from branch.models import Branch, BranchEmployee
from django.contrib import admin

@admin.register(Branch)
class BranchAdmin(admin.ModelAdmin):
    list_display = (
        'name', 
    )
    fields = (
        "name",
    )
    filters = (
        "name",
    )

@admin.register(BranchEmployee)
class BranchEmployeeAdmin(admin.ModelAdmin):
    list_display = (
        'branch',
        'employee' 
    )
    fields = (
        'branch',
        'employee' 
    )
    filters = (
        'branch',
        'employee' 
    )