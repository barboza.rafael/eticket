from accounts.models import User
from django.db import models


class Branch(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self) -> str:
        return f"{self.name}"

class BranchEmployee(models.Model):
    branch = models.ForeignKey(Branch, on_delete=models.CASCADE)
    employee = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    class Meta:
        ordering = ["branch__name"]
        verbose_name_plural = "Branchs employees"

    def __str__(self) -> str:
        return f"{self.branch} - {self.employee}"
