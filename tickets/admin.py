from tickets.models import Integration, Ticket, Category, TicketDetail, TicketStatus
from django.contrib import admin


class TicketDetailAdmin(admin.TabularInline):
    extra = 1
    model = TicketDetail
    list_display = (
        "ticket",
        "employee",
        "description"
        "create_at",
        "attached"
    )
    fields = (
        "ticket",
        "attached",
        "description",
        "employee"
    )
    list_filter = (
        "ticket",
    )


class TicketStatusAdmin(admin.TabularInline):
    model = TicketStatus
    extra = 1
    list_display = (
        "ticket",
        "status",
        "create_at",
        "responsible"
    )
    fields = (
        "ticket",
        "status",
        "responsible"
    )
    list_filter = (
        "ticket",
        "status",
        "create_at",
        "responsible"
    )


@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    list_display = (
        "client",
        "priority",
        "category",
        "employee",
        "create_at",
        "modified_at"
    )
    fields = (
        "priority",
        "client",
        "category",
        "issue",
        "employee"
    )
    list_filter = ("client", "category", "priority", "create_at",)
    inlines = [
        TicketStatusAdmin,
        TicketDetailAdmin,
    ]

@admin.register(Integration)
class IntegrationAdmin(admin.ModelAdmin):
    model = Integration
    extra = 1
    list_display = ("url",)
    fields = (
        "url",
        "request_payload",
        "response_payload"
    )

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("name",)
    fields = ("name", "icon", "user", "integration")
    list_filter = ("name", )
