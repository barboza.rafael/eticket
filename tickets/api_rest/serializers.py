from rest_framework import serializers

from tickets.models import Category, Ticket, TicketDetail, TicketStatus

from tickets.utils import SentIntegration

from django.core.mail import send_mail


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name', 'icon']
        read_only_fields = ['icon']


class TicketDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = TicketDetail 
        fields = ['ticket', 'id', 'description', 'create_at', 'attached', 'employee',]
        read_only_fields = ['create_at', 'ticket_id']
        extra_kwargs = {'ticket': {'required': False}}
    
    def create(self, validated_data):
        ticket_detail = super().create(validated_data)
        if self.context['request'].user.groups.filter(name="operators").exists():
            ticket_detail.ticket.employee = self.context['request'].user
            ticket_detail.ticket.save()
            ticket_detail.employee = self.context['request'].user
            ticket_detail.save()
        return ticket_detail

class TicketStatusSerializer(serializers.ModelSerializer):
    responsible = serializers.StringRelatedField()
    
    class Meta:
        model = TicketStatus
        fields = ['ticket', 'status', 'create_at', 'responsible']
        extra_kwargs = {'ticket': {'required': False}}
    
    def create(self, validated_data):
        ticketStatus = super().create(validated_data)
        self.send_email(ticketStatus.ticket.client.email, ticketStatus)
        return ticketStatus

    def send_email(self, to, ticketStatus):
        send_mail(
            f'Cambio de estado del ticket #{ticketStatus.ticket.id}',
            f'Este es un mensaje automatico en el cual se infoma que su ticket tiene el estado: {ticketStatus.status}',
            'Etickets@ticket.com',
            [to],
            fail_silently=False,
        )


class TicketSerializer(serializers.ModelSerializer):
    client_name = serializers.StringRelatedField(source='client')
    employee_name = serializers.StringRelatedField(source='employee')
    detail = TicketDetailSerializer(many=True, required=True)    
    status = TicketStatusSerializer(many=True, read_only=True)
    last_status = serializers.CharField(source='get_last_status', read_only=True)
    
    class Meta:
        model = Ticket
        fields = ['id', 'client', 'category', 'employee', 'issue', 'detail', 'status', 'last_status', 'create_at', 'client_name', 'employee_name']
        read_only_fields = ['create_at', 'modified_at', ]

    def create(self, validated_data):
        tickets_detail = validated_data.pop('detail')
        ticket = super().create(validated_data)
        tickets_detail_instances = list(map(lambda ticket_detail: TicketDetail(ticket=ticket, **ticket_detail), tickets_detail))
        TicketDetail.objects.bulk_create(tickets_detail_instances)
        TicketStatus.objects.create(ticket=ticket)
        SentIntegration(ticket)
        return ticket
