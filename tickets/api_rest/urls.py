from rest_framework import routers

from tickets.api_rest.views import TicketDetailViewSet, TicketViewSet, CategoryViewset, TicketStatusViewSet

router = routers.DefaultRouter()
router.register(r'tickets', TicketViewSet)
router.register(r'categories', CategoryViewset)
router.register(r'ticket-comment', TicketDetailViewSet, basename='ticket_comment')
router.register(r'ticket-status', TicketStatusViewSet, basename='ticket_status')
urlpatterns = router.urls
