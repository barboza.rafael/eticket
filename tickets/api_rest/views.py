from rest_framework import viewsets, authentication, permissions, parsers
from rest_framework_simplejwt.authentication import JWTAuthentication
from tickets.api_rest import paginations, serializers
from tickets.models import Category, Ticket, TicketDetail, TicketStatus


class CategoryViewset(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = serializers.CategorySerializer
    authentication_classes = [authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated]



class TicketViewSet(viewsets.ModelViewSet):
    queryset = Ticket.objects.all()
    serializer_class = serializers.TicketSerializer
    pagination_class = paginations.CustomPageNumbre
    authentication_classes = [authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated]
    parser_classes = [parsers.JSONParser, ]

    def get_serializer(self, *args, **kwargs):
        if kwargs.get('data'):
            kwargs['data']['client'] = self.request.user.id
        return super().get_serializer(*args, **kwargs)


class TicketDetailViewSet(viewsets.ModelViewSet):
    queryset = TicketDetail.objects.all()
    serializer_class = serializers.TicketDetailSerializer
    pagination_class = paginations.CustomPageNumbre
    authentication_classes = [authentication.SessionAuthentication, JWTAuthentication]
    permission_classes = [permissions.IsAuthenticated]
    parser_classes = [parsers.JSONParser, parsers.MultiPartParser,]
    http_method_names = ['post', 'patch']

class TicketStatusViewSet(viewsets.ModelViewSet):
    queryset = TicketStatus.objects.all()
    serializer_class = serializers.TicketStatusSerializer
    authentication_classes = [authentication.SessionAuthentication, JWTAuthentication]
    permission_classes = [permissions.IsAuthenticated]
    parser_classes = [parsers.JSONParser,]
    http_method_names = ['post',]