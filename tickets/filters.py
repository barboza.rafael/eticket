from django.db.models import fields
from tickets.models import Ticket
import django_filters

class TicketFilter(django_filters.FilterSet):
    category = django_filters.CharFilter(field_name='category__name')
    last_status = django_filters.CharFilter(method='get_last_status')
    class Meta:
        model = Ticket
        fields = ['category']
    
    def get_last_status(self, queryset, name, value):
        return queryset.filter(status__status=value)