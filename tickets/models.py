from django.db import models
from django.db.models.fields import IntegerField
from django.db.models.fields.related import ManyToManyField
from django.utils import tree
from accounts.models import User

class Integration(models.Model):
    url = models.URLField()
    request_payload = models.JSONField(null=True, blank=True)
    response_payload = models.JSONField(null=True, blank=True)
    status_code = models.IntegerField(null=True, blank=True)
    create_at = models.DateTimeField(auto_now_add=True, null=True)
    update_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self) -> str:
        return f"{self.url}"

class Category(models.Model):
    name = models.CharField(max_length=250)
    icon = models.CharField(max_length=200, null=True, blank=True)
    user = ManyToManyField(User, related_name='categories')
    integration = models.OneToOneField(Integration, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        verbose_name_plural = "categories"

    def __str__(self) -> str:
        return f"{self.name}"


class Ticket(models.Model):

    LOW = "LOW"
    MEDIUM = "MEDIUM"
    HIGH = "HIGH"
    PRIORITY = ((LOW, "low"), (MEDIUM, "medium"), (HIGH, "high"))

    client = models.ForeignKey(User, on_delete=models.CASCADE, related_name="client")
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    employee = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True, related_name="employee"
    )
    issue = models.CharField(max_length=250)
    priority = models.CharField(choices=PRIORITY, default=LOW, max_length=50)
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    @property
    def get_last_status(self):
        return self.status.first().status

    class Meta:
        ordering = ["-modified_at"]
        verbose_name_plural = "tickets"
        permissions = [
            ('can_see_other_ticket', 'Can see other tickets'), 
            ('can_assign_ticket', 'Can assign ticket'), 
            ('can_change_status_to_ticket', 'Can change status to ticket')
            ]


    def __str__(self) -> str:
        return f"{self.id}, {self.client}"


class TicketDetail(models.Model):
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE, related_name='detail')
    employee = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    description = models.TextField()
    create_at = models.DateTimeField(auto_now=True)
    attached = models.FileField(upload_to="uploads/%Y/%m/%d/", blank=True, null=True)

    class Meta:
        ordering = ["-create_at"]
        verbose_name_plural = "tickets details"

    def __str__(self) -> str:
        return f"{self.ticket}"


class TicketStatus(models.Model):
    OPEN = "OPEN"
    IN_PROGRESS = "IN_PROGRESS"
    SCALED = "SCALED"
    PENDING = "PENDING"
    SUCCESS = "SUCCESS"
    CLOSED = "CLOSED"
    REJECT = "REJECT"

    STATUS = (
        (OPEN, "Open"),
        (IN_PROGRESS, "In progress"),
        (SCALED, "Scaled"),
        (PENDING, "Pending"),
        (SUCCESS, "Success"),
        (CLOSED, "Closed"),
        (REJECT, "Reject"),
    )
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE, related_name='status')
    status = models.CharField(choices=STATUS, max_length=30, default=OPEN)
    create_at = models.DateTimeField(auto_now=True)
    responsible = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True, related_name="responsible"
    )

    class Meta:
        ordering = ["-create_at"]
        verbose_name_plural = "tickets status"

    def __str__(self) -> str:
        return f"{self.status}"