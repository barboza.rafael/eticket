from django.db.models.signals import post_save
from django.dispatch import receiver
from tickets.models import Ticket

# method for updating
@receiver(post_save, sender=Ticket, dispatch_uid="update_stock_count")
def update_stock(sender, instance, **kwargs):
    instance.product.stock -= instance.amount
    instance.product.save()