from collections import defaultdict
from accounts.models import User
from django.db.models import Count

from typing import List
from django import template
from tickets.models import Ticket, TicketStatus

register = template.Library()

@register.simple_tag
def get_ticket_close(tickets: Ticket) -> List:
    ticket_status = tickets.values('status__status', 'client')
    result = []
    ticket_status_list = defaultdict(list)
    temp_ticket = {}
    for ticket in ticket_status:
        ticket_status_list[ticket['status__status']].append(ticket)
    for ticket in ticket_status_list:
        temp_ticket[ticket] = len(ticket_status_list[ticket])
    for k,v in temp_ticket.items():
        result.append({"status__status": k, "total": v})
    return result


@register.simple_tag
def get_all_categories(tickets: Ticket) -> Ticket:
    category_status = tickets.values('category__name')
    result = []
    category_status_list = defaultdict(list)
    temp_category = {}
    for category in category_status:
        category_status_list[category['category__name']].append(category)
    for category in category_status_list:
        temp_category[category] = len(category_status_list[category])
    for k,v in temp_category.items():
        result.append({"category__name": k, "total": v})
    return result

@register.simple_tag
def get_my_ticket_count(user: User) -> Ticket:
    return user.client.all().count()