
from django.urls import path

from tickets.views import TicketsListView
 
urlpatterns = [
    path("", TicketsListView.as_view(), name='user_ticket'),
] 
