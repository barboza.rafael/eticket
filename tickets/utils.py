import requests
from tickets.api_rest import serializers

from tickets.models import Ticket
from rest_framework_simplejwt.tokens import RefreshToken

def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }

def SentIntegration(ticket: Ticket):
    integrations = ticket.category.integration
    try:
        if integrations:
            ticket_data = serializers.TicketSerializer(instance=ticket)
            tokens = get_tokens_for_user(ticket.client)
            response = requests.post(integrations.url, json={"msg":ticket_data.data, "tocken": tokens['access']})
            integrations.status_code = response.status_code
            integrations.request_payload = ticket_data.data
            integrations.response_payload = response.json()
            integrations.save()
    except Exception:
        pass
        