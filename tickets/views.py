from django.db.models.query_utils import Q
from tickets.filters import TicketFilter
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views import generic
from django_filters.views import FilterView

from tickets.models import Ticket


class TicketsListView(LoginRequiredMixin, FilterView):
    model = Ticket
    template_name = 'tickets/ticket.html'
    paginate_by = 10
    filterset_class = TicketFilter

    def get_queryset(self):
        query = super().get_queryset()
        user_group = self.request.user.groups.filter(name='operators')
        if user_group.exists():
            query = query.filter(category__in=self.request.user.categories.all()).filter(Q(employee=None) | Q( employee=self.request.user))
        else:
            query = query.filter(client=self.request.user)
        return query